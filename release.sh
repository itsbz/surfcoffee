#!/bi/bash

if yarn git-branch-is main; then
  (
    yarn bump
    git add package.json .onever.json front/package.json
    ver=$(node_modules/.bin/itsbz-onever get --machine)
    git commit -m "$ver"
    git tag "$ver"
    git push origin main --follow-tags
  )
fi
