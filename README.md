# SurfCoffee 

## Install
```shell
mkdir app 
cd app
git clone {URL} ./
``` 

## Run
```shell
( cd front && yarn install && yarn dev) &\
( cd back && yarn install && yarn dev) 
```
## Deploy
Use [netlify](https://app.netlify.com/sites/surfcoffee) deploy from repository.
To setupp follow this steps:
+ Add new deploy from GitLab repo
+ Specify base path as `front`
+ Other fields stay blanks because all configs already in `front/netlify.toml` file.
+ Commit changes to `main` branch
+ Run `/release.sh` script to release new version of site
+ Deploy must be started
