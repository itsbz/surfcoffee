import { register } from 'register-service-worker'
import { Notify } from 'quasar'
import axios from 'axios'

interface Manifest {
  version: string
}

const checkUpdates = async () => {
  const response = await axios.get<Manifest>(`/manifest.json?rnd=${Math.random()}`, { baseURL: '//' })
  const localVersion = localStorage.getItem('localVersion') || ''
  const remoteVersion = `${response.data.version}`
  if (localVersion !== remoteVersion) {
    localStorage.setItem('localVersion', remoteVersion)
    window.location.reload()
  }
}

register(process.env.SERVICE_WORKER_FILE, {
  // The registrationOptions object will be passed as the second argument
  // to ServiceWorkerContainer.register()
  // https://developer.mozilla.org/en-US/docs/Web/API/ServiceWorkerContainer/register#Parameter

  // registrationOptions: { scope: './' },

  ready() {
    if (process.env.DEV) {
      console.log('App is being served from cache by a service worker.')
    }
    setInterval(() => {
      checkUpdates().catch(() => console.error('Update check failed'))
    }, 60000)
  },

  registered(/* registration */) {
    if (process.env.DEV) {
      console.log('Service worker has been registered.')
    }
  },

  cached(/* registration */) {
    if (process.env.DEV) {
      console.log('Content has been cached for offline use.')
    }
  },

  updatefound(/* registration */) {
    if (process.env.DEV) {
      console.log('Обновление скачивается..')
    }
  },

  async updated() {
    console.log('New content is available; please refresh.')
    const response = await axios.get<Manifest>(`/manifest.json?rnd=${Math.random()}`, { baseURL: '//' })
    const localVersion = localStorage.getItem('localVersion') || ''
    const remoteVersion = `${response.data.version}`
    if (localVersion !== remoteVersion) {
      localStorage.setItem('localVersion', remoteVersion)
    }
    Notify.create({
      caption: 'Обновляемся..',
      message: `Вышла новая версия ${remoteVersion}`,
      color: 'primary',
      icon: 'announcement',
      onDismiss() {
        window.location.reload()
      },
    })
  },

  offline() {
    if (process.env.DEV) {
      console.log('No internet connection found. App is running in offline mode.')
    }
    Notify.create({
      message: 'Включен оффлайн режим',
      color: 'green',
    })
  },

  error(err) {
    if (process.env.DEV) {
      console.error('Error during service worker registration:', err)
    }
    Notify.create({
      message: `Error during service worker registration:${err.toString()}`,
      color: 'red',
    })
  },
})
