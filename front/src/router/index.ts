import { route } from 'quasar/wrappers'
import {
  createMemoryHistory, createRouter, createWebHashHistory, createWebHistory,
} from 'vue-router'
import { useUserStore } from 'src/store/user'
import routes from './routes'

/*
 * If not building with SSR mode, you can
 * directly export the Router instantiation;
 *
 * The function below can be async too; either use
 * async/await or return a Promise which resolves
 * with the Router instance.
 */

export default route(() => {
  const createHistory = process.env.SERVER
    ? createMemoryHistory
    : (process.env.VUE_ROUTER_MODE === 'history' ? createWebHistory : createWebHashHistory)

  const router = createRouter({
    // @ts-ignore
    scrollBehavior: (to, from, savedPosition) => {
      // eslint-disable-next-line no-restricted-globals
      if ('scrollRestoration' in history) {
        // eslint-disable-next-line no-restricted-globals
        history.scrollRestoration = 'manual'
      }
      let position = { x: 0, y: 0 }
      if (savedPosition) {
        // @ts-ignore
        position = savedPosition
      }
      return position
    },
    routes,

    // Leave this as is and make changes in quasar.conf.js instead!
    // quasar.conf.js -> build -> vueRouterMode
    // quasar.conf.js -> build -> publicPath
    history: createHistory(
      process.env.MODE === 'ssr' ? void 0 : process.env.VUE_ROUTER_BASE,
    ),
  })

  router.beforeEach((to, from, next) => {
    const userStore = useUserStore()

    /** Authenticated
     * at routes: /login
     * Redirect to /
     * */
    if (userStore.token && (to.path === '/login')) return next('/')

    /** User not authenticated
     * at routes: /profile,
     * Redirect to /login
     * */
    if (!userStore.token && to.path === '/profile') return next('/login')

    /**
     * User not Authenticated
     * at all protected routes (meta.requiresAuth) except /login
     * Redirect to /login
     * */
    if (!userStore.token && to.matched.some((record) => record.meta.requiresAuth)) return next('/login')

    return next()
  })

  return router
})
