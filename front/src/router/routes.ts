import { RouteRecordRaw } from 'vue-router'

const routes: RouteRecordRaw[] = [
  {
    path: '/',
    component: () => import('layouts/MainLayout.vue'),
    children: [
      {
        path: '',
        component: () => import('pages/Index.vue'),
      },
      {
        path: 'login',
        component: () => import('pages/Login.vue'),
      },
      {
        path: 'logout',
        component: () => import('pages/Logout.vue'),
        meta: { requiresAuth: true },
      },
      {
        path: 'profile',
        component: () => import('pages/Profile.vue'),
        meta: { requiresAuth: true },
      },
      {
        path: 'cart',
        component: () => import('pages/Cart.vue'),
      },
      {
        path: 'orders',
        component: () => import('pages/Orders.vue'),
        meta: { requiresAuth: true },
      },
      {
        path: 'order/:id',
        component: () => import('pages/Order.vue'),
        props: true,
        meta: { requiresAuth: true },
      },
      {
        path: 'about',
        component: () => import('pages/About.vue'),
      },
    ],
  },

  // Always leave this as last one,
  // but you can also remove it
  {
    path: '/:catchAll(.*)*',
    component: () => import('pages/Error404.vue'),
  },
]

export default routes
