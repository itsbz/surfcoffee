declare namespace NodeJS {
  interface ProcessEnv {
    NODE_ENV: string;
    VUE_ROUTER_MODE: 'hash' | 'history' | 'abstract' | undefined;
    VUE_ROUTER_BASE: string | undefined;
    APP_NAME: string;
    APP_DESC: string;
    APP_THEME_COLOR: string;
    API_URL: string;
  }
}
