import { defineStore } from 'pinia'
import { ref } from 'vue'
import { api } from 'boot/axios'

export interface Category {
  id: number
  name: string
  sort: number
}

export interface ModS {
  order?: number
  index?: number
  iiko_id: string
  name: string
  price: string
}

export interface ModG {
  is_matrix: number
  name: string
  modifiers: ModS[]
}

export interface Product {
  category_id: number
  full_desc: string
  id: string
  iiko_id: string
  image: string
  matrix: Record<string, ModS>
  name: string
  short_desc: string
  price: number
  grouped_modifiers: ModG[]
  simple_modifiers: ModS[]
}

export interface Price {
  [key: string]: number
}

export interface Menu {
  categories: Category[]
  prices: Price[]
  products: Product[]
}

export const useMenuStore = defineStore('menu', () => {
  const menu = ref({} as Menu)
  const selectedCategory = ref({ id: -1 } as Category)
  const selectedProduct = ref({} as Product)
  const isLoading = ref(false)

  const getBasePrice = (product: Product) => {
    let t = 0

    if (product.iiko_id !== '') {
      // @ts-ignore
      t = menu.value.prices[product.iiko_id as never]
    }

    if (product.grouped_modifiers.length > 1 && product.grouped_modifiers[0].is_matrix) {
      // @ts-ignore
      t = menu.value.prices[product.matrix['0_0'].iiko_id as never]
    } else if (product.grouped_modifiers[0]) {
      // @ts-ignore
      t = menu.value.prices[product.grouped_modifiers[0].modifiers[0].iiko_id as never]
    }
    return t
  }

  const getPriceOfMods = (product: Product, options: ModS[]) => {
    const matrix:number[] = [0, 0]
    let matrixSum = 0
    options.forEach((o) => {
      if (o.index !== undefined && o.order !== undefined) matrix[o.order] = o.index
    })
    options.forEach((o) => {
      if (o.index !== undefined && o.order !== undefined) { // @ts-ignore
        const id = product.matrix[matrix.join('_')]
        // @ts-ignore
        matrixSum = (menu.value.prices[id.iiko_id])
      }
    })
    let sum = 0
    options.forEach((o) => {
      sum += +o.price
    })
    return product.price + sum + matrixSum
  }

  const load = async () => {
    try {
      isLoading.value = true
      const res = await api.get('/menu/2')
      menu.value = res.data as Menu
      menu.value.products.forEach((p: Product) => {
        p.price = getBasePrice(p)
      })
      menu.value.products = menu.value.products.filter((i) => i.price)
      isLoading.value = false
    } catch (e) {
      console.error(e)
    }
  }

  const clear = () => {
    menu.value = { categories: [], prices: [], products: [] }
  }

  return {
    menu, selectedCategory, selectedProduct, isLoading, load, clear, getPriceOfMods,
  }
})
