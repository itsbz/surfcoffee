import { defineStore } from 'pinia'
import { ref } from 'vue'
import { api } from 'boot/axios'
import { Product } from 'src/store/menu'
import { SelectedGrouped, SelectedSimple } from 'src/store/product'
import { useUserStore } from 'src/store/user'

export interface CartItem extends Product{
  amount?: number
  selectedGrouped?: SelectedGrouped
  selectedSimple?: SelectedSimple
  UUID?: string
}

export const useCartStore = defineStore('cart', () => {
  const userStore = useUserStore()
  const isLoading = ref(false)
  const cart = ref([] as CartItem[])
  const orderInfo = ref({
    comment: '',
    time: '',
    door: false,
    bonus: 0,
  })

  const remove = (product: CartItem) => {
    cart.value = cart.value.filter((p) => p.UUID !== product.UUID)
  }

  const send = async () => {
    try {
      isLoading.value = true

      let items = ''
      cart.value.forEach((i) => {
        items += `+ ${i.amount} x ${i.name}\n`
        i.selectedGrouped.forEach((m:number, index) => {
          const modG = `  ${i.grouped_modifiers[index].name}: ${i.grouped_modifiers[index].modifiers[i.selectedGrouped[index]].name}`
          if (modG.trim() !== '') items += `${modG}\n`
        })
        const modS = `  ${i.simple_modifiers.filter((m, idx) => i.selectedSimple[idx] === 1).map((j) => j.name).join(', ')}`
        if (modS.trim() !== '') items += modS
      })

      let message = `Новый заказ\n${userStore.profile.name}\n+${userStore.profile.phone}`
      message += orderInfo.value.time !== '' ? `\nПриготовить к ${orderInfo.value.time}` : ''
      message += orderInfo.value.door ? '\nПринести заказ к двери' : ''
      message += orderInfo.value.comment !== '' ? `\nКомментарий: ${orderInfo.value.comment}` : ''
      message += `\n---\n${items}`
      const url = 'https://api.its.bz/v2/telegram/send?access-token=4pNVMVjhCAfXBgOLSMl84xyp8d663pDV'
      const data = {
        chatId: '-1001694880255',
        message,
      }
      const res = await api.post(url, data)
      isLoading.value = false
      return res
    } catch (e) {
      isLoading.value = false
      console.error(e)
      return false
    }
  }

  const clear = () => {
    cart.value = [] as CartItem[]
  }

  return {
    cart, orderInfo, isLoading, remove, send, clear,
  }
})
