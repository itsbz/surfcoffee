import { defineStore } from 'pinia'
import { ref } from 'vue'
import { api } from 'boot/axios'
import { Dark } from 'quasar'

export interface Result {
  error?: any
}

export interface Profile {
  balance: number
  birthday: string
  card: string
  email: string
  mode: number
  name: string
  orders: object[]
  phone: string
}

export interface SendSMSResult {
  state: string
  error?: any
}
export interface LoginResult {
  state?: string
  token?:string
  error?: any
}

export const useUserStore = defineStore('user', () => {
  const isLoading = ref(false)
  const token = ref('')
  const profile = ref({} as Profile)
  const settings = ref({
    dark: false,
  })

  const setDarkMode = () => {
    Dark.set(settings.value.dark)
  }

  const sendSMS = async (phone: string): Promise<SendSMSResult> => {
    isLoading.value = true
    try {
      const res = await api.post<SendSMSResult>('/login', { phone })
      if (!res.data.state && res.data.state !== 'code') res.data.error = 'ХЗ'
      isLoading.value = false
      return res.data
    } catch (error:any) {
      isLoading.value = false
      return { state: 'error', error }
    }
  }

  const login = async (phone: string, code: string): Promise<LoginResult> => {
    try {
      isLoading.value = true
      const res = await api.post<LoginResult>('/login', { phone, code })
      isLoading.value = false
      if (res.data.token) {
        token.value = res.data.token
        return { token: token.value }
      }
      if (res.data.state) {
        return { state: res.data.state, error: res.data.state }
      }
      return { error: 'ХЗ' }
    } catch (error:any) {
      isLoading.value = false
      return { state: 'error', error }
    }
  }

  const logout = () => {
    profile.value = {} as Profile
    token.value = ''
  }

  const loadProfile = async (): Promise<Result> => {
    try {
      isLoading.value = true
      const res = await api.post<Profile>('/getClientByToken', { token: token.value })
      isLoading.value = false
      if (res.data.name) {
        profile.value = res.data
        return {}
      } return { error: res.data }
    } catch (error: any) {
      isLoading.value = false
      return { error }
    }
  }

  const updateProfile = async (newProfile: Profile): Promise<Result> => {
    try {
      isLoading.value = true
      const res = await api.post<{status: string}>('/updateClientByToken', { ...newProfile, balance: 666, token: token.value })
      isLoading.value = false
      if (res.data.status && res.data.status === 'success') return {}
      return { error: res.data }
    } catch (error: any) {
      isLoading.value = false
      return { error }
    }
  }

  return {
    isLoading,
    profile,
    settings,
    token,
    setDarkMode,
    sendSMS,
    login,
    logout,
    loadProfile,
    updateProfile,
  }
})
