import { defineStore } from 'pinia'
import { ref } from 'vue'
import { Product, useMenuStore } from 'src/store/menu'
import { CartItem, useCartStore } from 'src/store/cart'

export type SelectedSimple = number[]
export type SelectedGrouped = number[]

export const useProductStore = defineStore('product', () => {
  const isLoading = ref(false)
  const currentProd = ref({} as Product)
  const selectedSimple = ref([0, 0] as SelectedSimple)
  const selectedGrouped = ref([0, 0] as SelectedGrouped)
  const menuStore = useMenuStore()
  const cartStore = useCartStore()
  const prices = menuStore.menu.prices

  const initProduct = (product:Product) => {
    currentProd.value = product
    selectedSimple.value = []
    selectedGrouped.value = []
    currentProd.value.grouped_modifiers.forEach(((prod, index) => {
      if (prod.name !== 'Сироп') selectedGrouped.value[index] = 0
    }
    ))
    if (currentProd.value.iiko_id !== '') {
      currentProd.value.price = prices[currentProd.value.iiko_id]
      if (currentProd.value.grouped_modifiers.length > 1
        && currentProd.value.grouped_modifiers[0].is_matrix) {
        currentProd.value.price = prices[currentProd.value.matrix['0_0'].iiko_id]
      } else if (currentProd.value.grouped_modifiers[0] !== undefined) {
        const priceId = currentProd.value.grouped_modifiers[0].modifiers[0].iiko_id
        currentProd.value.price = prices[priceId]
      }
    }
  }

  const toggleSimple = (index:number) => {
    const priceId = currentProd.value.simple_modifiers[index].iiko_id
    if (selectedSimple.value[index]) {
      delete selectedSimple.value[index]
      currentProd.value.price -= prices[priceId]
    } else {
      selectedSimple.value[index] = 1
      currentProd.value.price += prices[priceId]
    }
  }

  const calculatePrice = (groupIndex: number, modIndex: number) => {
    if (currentProd.value.iiko_id !== '') currentProd.value.price = prices[currentProd.value.iiko_id]
    if (currentProd.value.grouped_modifiers.length > 1
        && currentProd.value.grouped_modifiers[0].is_matrix) {
      currentProd.value.price = prices[currentProd.value.matrix[`${selectedGrouped.value[0]}_${selectedGrouped.value[1]}`].iiko_id]
    } else {
      currentProd.value.price = prices[currentProd.value.grouped_modifiers[groupIndex].modifiers[modIndex].iiko_id]
      if (selectedGrouped.value[2] !== null
          && selectedGrouped.value[2] !== null
          && currentProd.value.grouped_modifiers[2] !== undefined) {
        const priceId = currentProd.value.grouped_modifiers[2].modifiers[modIndex].iiko_id
        currentProd.value.price += prices[priceId]
      }
      if (selectedGrouped.value[2] === null && currentProd.value.grouped_modifiers[2] !== undefined) {
        const priceId = currentProd.value.grouped_modifiers[groupIndex].modifiers[modIndex].iiko_id
        currentProd.value.price -= prices[priceId]
      }
    }
  }

  const toggleGrouped = (groupIndex:number, modIndex: number) => {
    if (selectedGrouped.value[groupIndex] !== null && modIndex === selectedGrouped.value[groupIndex] && groupIndex > 1) {
      delete selectedGrouped.value[groupIndex]
    } else {
      selectedGrouped.value[groupIndex] = modIndex
      if (Object.keys(currentProd.value.matrix).length > 0) {
        const priceId = currentProd.value.matrix[`${selectedGrouped.value[0]}_${selectedGrouped.value[1]}`].iiko_id
        currentProd.value.price = prices[priceId]
      } else {
        const priceId = currentProd.value.grouped_modifiers[groupIndex].modifiers[modIndex].iiko_id
        currentProd.value.price = prices[priceId]
      }
    }
    calculatePrice(groupIndex, modIndex)
  }

  const addToBasket = () => {
    const cart = cartStore.cart
    const prod: CartItem = { ...currentProd.value }
    const getUUID = (pr: CartItem) => `${pr.id}_${pr.selectedSimple.join(',')}_${pr.selectedGrouped.join(',')}`
    prod.selectedGrouped = selectedGrouped.value
    prod.selectedSimple = selectedSimple.value
    prod.amount = 1
    prod.UUID = getUUID(prod)

    const pr = cart.filter((i) => i.UUID === prod.UUID)[0] || undefined
    if (pr) pr.amount += 1
    else cart.push(prod)
  }

  return {
    addToBasket, toggleGrouped, toggleSimple, initProduct, calculatePrice, isLoading, currentProd, selectedSimple, selectedGrouped,
  }
})
