import { store } from 'quasar/wrappers'
import { createPinia, Pinia as PiniaType } from 'pinia'
import { unref, Ref } from 'vue'
import { createPersistedStatePlugin } from 'pinia-plugin-persistedstate-2'

declare module '@quasar/app' {
  interface QSsrContext {
    state: Ref<never> | never
  }
}

// provide typings for `this.$store`
declare module '@vue/runtime-core' {
  interface ComponentCustomProperties {
    $store: PiniaType
  }
}

declare module 'pinia' {
  interface Pinia {
    // eslint-disable-next-line no-unused-vars
    replaceState(state: never): void
  }
}

export default store(({ ssrContext }: any) => {
  const pinia = createPinia()
  pinia.use(createPersistedStatePlugin())
  if (process.env.SERVER && ssrContext) {
    // eslint-disable-next-line @typescript-eslint/no-unsafe-call
    ssrContext.onRendered(() => {
      // unwrapping the state for serialization
      // eslint-disable-next-line @typescript-eslint/no-unsafe-assignment
      ssrContext.state = unref(ssrContext.state)
    })
  }
  if (process.env.CLIENT) {
    pinia.replaceState = (state: never) => {
      pinia.state.value = state
    }
  }
  return pinia
})
